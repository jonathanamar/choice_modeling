import numpy as np
import torch

identity_function = lambda x:x
BASE_NPR = .01

class REV_CNL():
	def __init__(self, cnl_model, units_map_index,
				 util_tensor, price_tensor, presence_tensor,
				 price_transformation = identity_function,
				 no_purchase_rate = BASE_NPR, volume_price_balance = 0.,
				 index_nz = 0, 
				 competition_mask = None
				 ):
		self.cnl_model = cnl_model
		self.cnl_model.requires_grad_(False)
		self.util_tensor = util_tensor
		self.price_tensor= price_tensor
		self.presence_tensor = presence_tensor
		self.units_map_index = units_map_index
		self.index_nz = index_nz
		
		self.units_nz = sorted(list(self.units_map_index.keys()))[self.index_nz:]
		self.set_npr(no_purchase_rate)
		self.set_vpb(volume_price_balance, price_transformation)
		self.get_matrix_cstr()
		self.set_competition_mask(competition_mask)
		
	
	def get_presence_from_assortment(self, assortment):
		assortment_presence = np.zeros(self.presence_tensor.shape)
		for i in assortment+[0]: assortment_presence[0, self.units_map_index[i]] = 1.
		return assortment_presence
	
	def pred_revenue(self, assortment, **kwargs):
		assortment_presence = self.get_presence_from_assortment(assortment)
		return self.revenue_from_presence(torch.Tensor(assortment_presence), agg=True, **kwargs).item()


	def market_share_from_presence(self, presence, agg=False):
		ms = self.cnl_model.forward_from_utils(self.util_npr + (presence - 1)*C )
		if agg:
			return ms.sum(1)
		else:
			return ms
	  
	def revenue_from_presence(self, presence, 
		agg =True, objective = 'obj', competition_mask = None):
		#Predict the objective from a presence tensor, in 0,1
		ms = self.market_share_from_presence(presence)
		
		#Apply competition mask
		competition_mask = competition_mask if (competition_mask is not None) else self.competition_mask
		ms_non_competition = ms * competition_mask

		if objective == 'obj':
			revs = ms_non_competition * (self.objective_vpb)
		elif objective == 'ms':
			revs = ms_non_competition
		elif objective == 'rv':
			revs = ms_non_competition * self.price_tensor
		else:
			raise ValueError("'objective' must be among ['obj', 'rv', 'ms']")

		if agg:
			return revs.sum(1)
		else:
			return revs


	def revenue_from_facings(self, facings):
		return
		

	def revenue_from_presence_nz(self, presence, **kwargs):
		"""will be deprecated"""
		presence_f = torch.cat([torch.ones(presence.shape[0], self.index_nz), presence], 1)
		return self.revenue_from_presence(presence_f, **kwargs)

	
	def set_npr(self, npr):
		self.npr = npr
		self.zero_util = torch.Tensor([np.log(1/BASE_NPR - 1) - np.log(1/self.npr - 1)]).unsqueeze(1)
		self.util_npr = torch.cat((self.zero_util, self.util_tensor[:,1:]), 1)

	def set_vpb(self, vpb=None, price_transformation = None):
		self.price_max = self.price_tensor.max()
		if (vpb is not None): self.vpb = vpb
		if (price_transformation is not None): self.price_transformation = price_transformation
		
		self.transformed_prices = self.price_transformation(self.price_tensor+1e-10) / self.price_transformation(self.price_max)
		self.objective_vpb = (self.transformed_prices * (1 - self.vpb) + self.vpb)


	def set_competition_mask(self, competition_mask = None):
		#Default behavior is competition_mask is product with negative prices
		if competition_mask is None:
			self.competition_mask = (self.price_tensor>0.) * 1.
		else:
			self.competition_mask = competition_mask



		
	def get_matrix_cstr(self, L = 0):
		"""Get matrix constraints from current assortment, with flexibility level L in the form Ax < b
		  constraints are assortment size and intersection with current assortment
		"""
		l_cur = int(self.presence_tensor[0,self.index_nz:].sum().item())
		r = self.presence_tensor[0,self.index_nz:].detach().numpy()
		self.assortment_cur = [self.units_nz[i] for (i,u) in enumerate(r) if u == 1]
		A = np.array([np.ones(len(self.units_nz)), -r, 1-r])
		b = np.array([l_cur, -(l_cur - L), L])
		return A,b, self.assortment_cur.copy(), l_cur


	@classmethod
	def from_index(cls, cnl_model, store_ind, units_map_index,
			*args_tensor, **kwargs):
		args_tensor_selected = tuple([t[[store_ind]] for t in args_tensor])
		return cls(cnl_model, units_map_index, *args_tensor_selected, **kwargs)




class REV_CNL_from_index(REV_CNL):
	def __init__(self, cnl_model, store_ind, units_map_index,
			*args_tensor, **kwargs):
		args_tensor_selected = tuple([t[[store_ind]] for t in args_tensor])
		super(REV_CNL_from_index, self).__init__(
			cnl_model, units_map_index,
			*args_tensor_selected,
			**kwargs
		)	 


