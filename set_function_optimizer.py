import numpy as np
import numpy.random as rd
import random


class zero_function_optimizer():
	"""class for zero one set function optimization, takes as input the function and the total set for indexing"""
	def __init__(self, f, set_map, **kwargs):
		"Inputs the set function, and set of units. kwargs are extra for the function"
		self.f = lambda x: f(x, **kwargs)
		self.set_map = set_map
		self.dim = len(self.set_map)


	def S_to_x(self, S):
		"""convert set to vector"""
		return np.array([1. * (u in S) for u in self.set_map])
	
	def x_to_S(self, x):
		"""convert binary vector to set"""
		return [self.set_map[i] for i in range(self.dim) if x[i]==1.]

	def get_samples(self, x, t, block, min_samples = 1):
		#Return t + min_samples*2*block samples, where only the first t are unconditionnal and can be recycled based on values

		samples = ((np.random.rand(2*len(block)*min_samples,self.dim) - x) < 0) * 1.
		base_block = np.eye(self.dim)[block,:]
		base_block = np.concatenate([base_block, -base_block])
		samples += np.tile(base_block, (min_samples,1) )
		samples = samples.clip(0,1)

		samples = np.concatenate([
			((np.random.rand(t,self.dim) - x) < 0) * 1.,
			samples
		])
		return samples
	

	def F_gradient_sup(self, x, t, block, min_samples = 1):
		"""
		basic version of gradient of F
		each coordinate of the gradient is essentially the difference in conditional expectations (property of multilinear extension)
		this is done by setting x_i to 0 or 1
		"""
		samples = self.get_samples(x, t, block, min_samples = min_samples)
		
		#samples_values = np.array([self.f(self.x_to_S(samples[i,:])) for i in range(samples.shape[0])])
		samples_values = self.f(torch.Tensor(samples))
		
		#Count the expectations values for the last coordinates, one value per block sign sample
		#Then recycle the first t components
		g = samples_values[t:].reshape((min_samples, 2*len(block))).sum(0)
		gp, gn = g[:len(block)], g[len(block):]
		gp_dim = np.zeros(self.dim); gp_dim[block] = gp
		gn_dim = np.zeros(self.dim); gn_dim[block] = gn
		
		F_grad = (
			samples[:t].transpose().dot(samples_values[:t]) + gp_dim
		) / (samples[:t].sum(0) + 1e-10 + min_samples) - (
			 (1-samples[:t]).transpose().dot(samples_values[:t]) + gn_dim
		) / ((1-samples[:t]).sum(0) + 1e-10 + min_samples)
		#F_grad = samples.transpose().dot(samples_values) / (samples.sum(0)+1e-10) -\
		#		(1-samples).transpose().dot(samples_values) / ((1-samples).sum(0)+1e-10)

		non_block = [i for i in range(self.dim) if i not in block]
		F_grad[non_block] = 0. 
		return F_grad


	def frank_wolf(self, x_init, A, b, 
		max_iter = 100, print_ = False, size_block = 0, min_samples = 1,
		allowed_block = None, t = 100, 
		**kwargs
		):
		"""
		FW algorithm
		x_init initial start
		A,b matrix constraints: constraints Ax < b (not containing 0<x<1)
		block size for gradient estimation
		t number of samples in estimation of F
		"""
		x_j = x_init
		grad = np.zeros(self.dim)
		if allowed_block is None: allowed_block = list(range(self.dim))

		for j in range(1,max_iter+1):
			# Block coordinate descent - update gradient estimate or set 0
			block = random.sample(allowed_block, size_block) if ((size_block > 0) & (j > 1)) else allowed_block
			grad_block = self.F_gradient_sup(x_j, t, block, min_samples = min_samples)
			grad = np.array([grad_block[i] if i in block else grad[i] for i in range(self.dim)])
			#if print_: print("iter:", j, "grad: ", grad)
			
			#step1
			z_j = self.get_argmax_size_cstr(grad, A, b)
			
			#step2: stepsize - convex combination
			gamma = self.step_size(j)
			x_j = gamma*z_j + (1-gamma)*x_j

			if print_: print("iter: ",j,"\n","x: ", x_j,"\n", "z: ",z_j,"\n", "gradient: ", grad,"\n", "gamma: ", gamma,"\n")

		return x_j	

	def get_argmax_size_cstr(self, grad, A,b):
		"""for the current constraints, the projection can be solved by sorting
		find optimal value: max_x grad*x s.t. Ax<b and x>0 and x<1
		"""
		x_opt = np.zeros(self.dim)

		grad_old_assortment = [grad[i] for i in range(self.dim) if A[1][i] == -1]
		index_old_assortment= [i for i in range(self.dim) if A[1][i] == -1]
		for i in np.argsort(grad_old_assortment)[::-1][:-b[1]]: 
			x_opt[index_old_assortment[i]] = 1.

		grad_remainder = [grad[i] for i in range(self.dim) if x_opt[i] == 0]
		index_remainder= [i for i in range(self.dim) if x_opt[i] == 0]
		for i in np.argsort(grad_remainder)[::-1][:b[2]]:
			if grad_remainder[i] <= 0: break
			x_opt[index_remainder[i]] = 1.	
		
		return x_opt


	def x_init_from_x(self, x, A, b):
		x_init = self.get_argmax_size_cstr(x, A, b)	
		return x_init

	def x_init_rand_w(self, A, b, m = 0.):
		"""get random feasible x
		using some generative model
		x_base is a balanced across units (equal)
		x_init is a random feasible assortment
		"""
		x_init = np.zeros(self.dim)
		ass = random.sample([i for i in range(self.dim) if A[1][i] == -1], -b[1]) \
			+ random.sample([i for i in range(self.dim) if A[2][i] ==	1],	b[2])
		for i in ass: x_init[i] = 1.
		x_base = np.zeros(self.dim)
		x_base[A[1] == -1] = b[1] / A[1].sum()
		x_base[A[2] ==	1] = b[2] / A[2].sum()
		
		return x_init * m + (1-m) * x_base
	

	@staticmethod
	def step_size(j):
		"""helper function for step size"""
		return 2/(2+j)



"""Unconstrainted optimization"""
class zero_function_optimizer_full(zero_function_optimizer):
	def get_argmax_size_cstr(self, grad, A,b):
		x_opt =(grad>0)*1.; return x_opt
	def x_init_from_x(self, x, A, b, threshold = .5):
		return self.get_argmax_size_cstr(x-threshold, A, b) 


"""Gradient known"""
class zero_function_optimizer_grad(zero_function_optimizer):
"""class for zero one set function optimization, takes as input the function and the total set for indexing"""
	def __init__(self, f, set_map, f_grad):
		super(zero_function_optimizer_grad, self).__init__(f, set_map)
		self.f_grad = f_grad

	def F_gradient_sup(self, x, **kwargs):
		return self.f_grad(x)


class zero_function_optimizer_LP(zero_function_optimizer):
	def __init__(self, f, set_map, A = None, b = None):
		super(zero_function_optimizer_LP, self).__init__(f,set_map)
		self.set_optimizer_ = False
		if (A is not None) and (b is not None):
			self.set_optimizer(A,b)

	def set_optimizer(self, A, b):
		self.m = pulp.LpProblem("projection", pulp.LpMaximize);
		self.A = A; self.b = b; self.size_con, self.dim = A.shape;
		self.z = pulp.LpVariable.dicts("G",range(self.dim),0,1)
		
		self.m.setObjective( pulp.lpSum([self.z[i] for i in range(self.dim)]))
		for i in range(self.size_con):
			self.m += pulp.lpSum([self.A[i][j] * self.z[j] for j in range(self.dim)])<=self.b[i], "C_%s"%str(i)
		self.set_optimizer_ = True
		return self.m
	
	def get_argmax_size_cstr(self, grad, A,b):
		if self.set_optimizer_ == False:
			self.set_optimizer(A, b)

		self.m.setObjective(pulp.lpSum([grad[i]*self.z[i] for i in range(self.dim)]))
		self.m.solve()
		x_opt = np.array([self.z[i].value() for i in range(self.dim)])
		return x_opt


class zero_function_optimizer_GRB(zero_function_optimizer):
	def __init__(self, f, set_map, A = None, b = None):
		super(zero_function_optimizer_GRB, self).__init__(f,set_map)
		self.set_optimizer_ = False
		if (A is not None) and (b is not None):
			self.set_optimizer(A,b)

	def frank_wolf(self, x_init, A, b, **kwargs):
		self.set_optimizer(A, b)
		return super(zero_function_optimizer_GRB, self).frank_wolf(x_init, A, b, **kwargs)
	
	def set_optimizer(self, A, b):
		self.m = gp.Model('LP');
		self.A = A; self.b = b; self.size_con, self.dim = A.shape;
		self.z = []
		for j in range(self.dim): 
			self.z.append(self.m.addVar(lb =0., ub = 1., name = 'z_'+str(j)))

		for i in range(self.size_con):
			expr = gp.LinExpr()
			for j in range(self.dim):
				if A[i][j] != 0:
					expr += A[i][j] * self.z[j]
			self.m.addConstr(expr, GRB.LESS_EQUAL, b[i])
		self.set_optimizer_ = True
		return m
	
	def get_argmax_size_cstr(self, grad, A,b):
		if self.set_optimizer_ == False:
			self.set_optimizer(A, b)

		expr = gp.LinExpr()
		for i in range(self.dim):
			if grad[i] != 0:
				expr += grad[i] * self.z[i]
		self.m.setObjective(expr, GRB.MAXIMIZE)
		self.m.optimize()
		x_opt = np.array([self.z[i].x for i in range(self.dim)])
		return x_opt




class set_function_optimizer(zero_function_optimizer):
	"""class for set function optimization, takes as input the function and the total set for indexing"""
	def __init__(self, f, set_map):
		super(set_function_optimizer, self).__init__(f,set_map)		
	
	
	def f_to_F(self, x, t):	
		"""convert set function f to function F on R^n, and apply to vector x
		using stochastic version of multilinear extension
		i.e. 
		use x as probailities of selecting a unit of set 
		t is the number of sampled sets
		return estimate of extension on x
		"""
		sum_R = 0	
		for i in range(t):
			x_bar = np.random.uniform(0,1, x.shape)
			r_t = x >= x_bar
			R_t = self.x_to_S(1*r_t)

			sum_R = sum_R + self.f(R_t)
		estimation = sum_R/t
		
		return estimation
	 
	def F_gradient(self, x, t):
		"""
		basic version of gradient of F
		each coordinate of the gradient is essentially the difference in conditional expectations (property of multilinear extension)
		this is done by setting x_i to 0 or 1
		"""
		F_grad = []
		
		start = time.time()
		for i in range(self.dim):
			x_without_i = copy.deepcopy(x)
			x_without_i[i] = 0.0

			x_with_i = copy.deepcopy(x)
			x_with_i[i] = 1.0

			df_dxi = self.f_to_F(x_with_i, t) - self.f_to_F(x_without_i, t)
			print("coord i:%s \t df_dxi: %s"%(str(i), str(df_dxi)))
			end = time.time();print(end-start);start = time.time()

			F_grad.append(df_dxi)

		return np.array(F_grad).reshape(x.shape)

	def F_gradient_correlated(self, x, t, block):
		"""
		gradient estimation, using correlated samples across coordinates,
		gradient is estimated only on the coordinates in block
		returns gradient
		"""
		F_grad = np.zeros(np.shape(x))
		
		for _ in range(t):
			x_bar = np.random.uniform(0,1, x.shape) 
			r_t = x >= x_bar
			R_t = self.x_to_S(1*r_t)
			val_R_t = self.f(R_t)

			F_grad_t = []

			for i in range(self.dim):
			if i in block:		
				r_t_other = copy.deepcopy(r_t)
				if r_t[i] == 1:
				r_t_other[i] = 0
				R_t_other = self.x_to_S(1*r_t_other)
				val_R_t_other = self.f(R_t_other)
				grad_r_t_i = val_R_t - val_R_t_other
				F_grad_t.append(grad_r_t_i)
				elif r_t[i] == 0: 
				r_t_other[i] = 1
				R_t_other = self.x_to_S(1*r_t_other)
				val_R_t_other = self.f(R_t_other)
				grad_r_t_i = val_R_t_other - val_R_t
				F_grad_t.append(grad_r_t_i)
				
			else:
				F_grad_t.append(0)

			F_grad = F_grad + np.array(F_grad_t).reshape(np.shape(x))
			
		return F_grad/t

	def F_BC_gradient(self, x, t, block):
		'''Block coordinate gradient of F
			weaker version of previous
		'''
		F_grad = []
		for i in range(self.dim):
			if i in block:
			x_without_i = copy.deepcopy(x)
			x_without_i[i] = 0.0

			x_with_i = copy.deepcopy(x)
			x_with_i[i] = 1.0

			df_dxi = self.f_to_F(x_with_i, t) - self.f_to_F(x_without_i, t)
			print("coord i:%s \t df_dxi: %s"%(str(i), str(df_dxi)))
			else:
			df_dxi = 0

			F_grad.append(df_dxi)

		return np.array(F_grad).reshape(x.shape)
	
	