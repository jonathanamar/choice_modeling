import random
import tqdm
import copy
import torch




class Overfit_trainer():
	def __init__(
		self, model, criterion, optimizer, 
		df_sales_tensor, sales_col_tensor,
	):
		self.model = model
		self.criterion = criterion
		self.optimizer = optimizer
		self.df_sales_tensor = df_sales_tensor
		self.sales_col_tensor = sales_col_tensor
	

	def step(self, x, y):
		self.optimizer.zero_grad()
		y_hat = self.model(x)
		cur_loss = self.criterion(self.model, y_hat, y)
		cur_loss.backward()
		self.optimizer.step()
		return cur_loss

		
	def fit_model(self,N_ITERS):
		pbar = range(N_ITERS)
		for epoch in pbar:
			x = self.df_sales_tensor
			y = self.sales_col_tensor
			cur_loss = self.step(x, y)




class Batch_trainer(Overfit_trainer):
	def __init__(
		self, model, criterion, optimizer, 
		df_sales_tensor, sales_col_tensor,
		training_indices,
	):
		super(Batch_trainer, self).__init__(
			model, criterion, optimizer, df_sales_tensor, sales_col_tensor,
		)
		self.training_indices = copy.copy(training_indices)

	def select_indices(self, a):
		x = self.df_sales_tensor[a]
		y = self.sales_col_tensor[a]
		return x,y		
		
	def train(self, BATCH_SIZE, N_EPOCHS, tqdm_ = False):
		pbar = range(N_EPOCHS) if not tqdm_ else tqdm.trange(N_EPOCHS)
		for epoch in pbar:  # loop over the dataset multiple times
			running_loss = self.pass_data(BATCH_SIZE)
			loss_description = "[E%d] -  train: %.5e" % (epoch, running_loss)
			_ = print(" ---- ", loss_description) if not tqdm_ else pbar.set_description(loss_description)
		return model			


	def pass_data(self, BATCH_SIZE):
		#Passes through data, taking stepm and shuffling at the beginning
		running_loss = 0.0
		random.shuffle(self.training_indices)
		for i in range(len(self.training_indices)//BATCH_SIZE):
			a = self.training_indices[i*BATCH_SIZE:(i+1)*BATCH_SIZE]
			x,y = self.select_indices(a)			
			cur_loss = self.step(x, y)
			running_loss += cur_loss.item()
		return running_loss / max(len(self.training_indices)//BATCH_SIZE, 1)




class SGD_trainer(Batch_trainer):
	def __init__(
		self, model, criterion, optimizer, 
		df_sales_tensor, sales_col_tensor,
		training_indices, validation_indices, max_patience = 1
	):
		super(SGD_trainer, self).__init__(
			model, criterion, optimizer, df_sales_tensor, sales_col_tensor, training_indices,
		)
		self.validation_indices = copy.copy(validation_indices)
		self.max_patience = max_patience
		self.count_patience = 0
		self.x_val, self.y_val = self.select_indices(self.validation_indices)
		self.cur_val_loss = self.evaluate_validation_loss()


	def evaluate_validation_loss(self):
		return self.criterion(self.model, self.model(self.x_val), self.y_val).item()
	
	
	def test_validation_loss(self):
		val_loss_tested = self.evaluate_validation_loss()
		if val_loss_tested > self.cur_val_loss: 
			self.count_patience += 1
		else:
			self.cur_val_loss = val_loss_tested
			self.count_patience = 0

	
	def train(self, BATCH_SIZE, N_EPOCHS, tqdm_ = False):
		pbar = range(N_EPOCHS) if not tqdm_ else tqdm.trange(N_EPOCHS)
		for epoch in pbar:
			self.test_validation_loss()
			if self.count_patience >= self.max_patience: break
			
			running_loss = self.pass_data(BATCH_SIZE)
			loss_description = "[E%d] -  train: %.5e - val: %.5e" % (epoch, running_loss, self.cur_val_loss)
			_ = print(" ---- ", loss_description) if not tqdm_ else pbar.set_description(loss_description)




class row_selector_sparse():
    def __init__(self, df_sales_tensor_sparse):		
        self.coordinates = {i: [] for i in range(df_sales_tensor_sparse.shape[0])}
        for (i,v) in enumerate(df_sales_tensor_sparse._indices()[0].numpy()): self.coordinates[v] += [i]
        self.indices = df_sales_tensor_sparse._indices()
        self.values = df_sales_tensor_sparse._values()
        self.size = [i for i in df_sales_tensor_sparse.shape]

    def select(self, stores_sample):
        stores_sample_index = {k:i for (i,k) in enumerate(stores_sample)}
        ind_select = [c for s in stores_sample for c in self.coordinates[s]]
        return \
        torch.sparse.FloatTensor(
            torch.cat(
                [self.indices[0][ind_select].apply_(lambda x: stores_sample_index[x]).unsqueeze(0)] +
                [l[ind_select].unsqueeze(0) for l in self.indices[1:]]
                , 0),
            self.values[ind_select],
            torch.Size([len(stores_sample)] + self.size[1:])
        )	




"""Multi inheritance for sparse class"""	
class trainer_sparse():
	def __init__(
		self, df_sales_tensor_sparse, sales_col_tensor_sparse,
	):	
		self.df_sales_selector  = row_selector_sparse(df_sales_tensor_sparse)
		self.sales_col_selector = row_selector_sparse(sales_col_tensor_sparse)

	def select_indices(self, a):
		x = self.df_sales_selector.select(a).to_dense()
		y = self.sales_col_selector.select(a).to_dense().squeeze(2)
		return x,y	

class Batch_trainer_sparse_m(trainer_sparse, Batch_trainer):
	def __init__(
		self, model, criterion, optimizer, 
		df_sales_tensor_sparse, sales_col_tensor_sparse,
		training_indices,
	):	
		trainer_sparse.__init__(self, df_sales_tensor_sparse, sales_col_tensor_sparse)
		Batch_trainer.__init__(self,
			model, criterion, optimizer, 
			df_sales_tensor_sparse, sales_col_tensor_sparse,
			training_indices,
		)

class SGD_trainer_sparse_m(trainer_sparse, SGD_trainer):
	def __init__(
		self, model, criterion, optimizer, 
		df_sales_tensor_sparse, sales_col_tensor_sparse,
		training_indices, validation_indices, max_patience = 1
	):	
		trainer_sparse.__init__(self, df_sales_tensor_sparse, sales_col_tensor_sparse)
		SGD_trainer.__init__(self,
			model, criterion, optimizer, 
			df_sales_tensor_sparse, sales_col_tensor_sparse,
			training_indices, validation_indices, max_patience
		)	




