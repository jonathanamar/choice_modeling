import torch
import torch.nn as nn
from torch.nn import init


C = 1e10

class LinearLayer(nn.Module):
	def __init__(self, in_features, units, C = C):
		super(LinearLayer, self).__init__()
		self.units = units
		self.in_features = in_features
		self.num_units = len(self.units)
		self.num_features = len(self.in_features)
		
		self.beta_feat = nn.Parameter(torch.rand(self.num_features, 1) / (self.num_features**3))
		self.beta_dummy= nn.Parameter(torch.zeros(self.num_units - 1,1))#torch.rand(self.num_units - 1,1) / (self.num_units**3)
		self.get_dummies()
		self.C = C

	def get_dummies(self):
		self.dummies = torch.cat((torch.zeros(1,1), self.beta_dummy), 0)
		return self.dummies

	def forward(self, input_tensor):
		#input tensor of size NUM_INDICES * units * features
		#first column is presence binary
		self.get_dummies()
		util_dummy= (input_tensor[:,:,0]  * self.dummies.reshape(1,-1))#.unsqueeze(2)
		util_feat = (input_tensor[:,:,1:] * self.beta_feat.reshape(1,1,-1)).sum(2)
		utilities = util_dummy + util_feat + self.C * (input_tensor[:,:,0] - 1)
		return utilities

	def extra_repr(self):
		return 'units={}, features={}'.format(
			self.num_units, self.num_features
		)


class NestingLayer(nn.Module):
	#MU <=1 using sigmoid mapping
	def __init__(self, in_features, units, memberships, C = C):
		super(NestingLayer, self).__init__()
		self.num_units, self.num_nests = memberships.shape
		self.memberships = torch.Tensor(memberships)
		
		self.mu_param = nn.Parameter(torch.rand(self.num_nests))
		self.get_mu()
		#self.mu_clamp = self.mu.clamp_max(1.)
		self.logsoftmax = nn.LogSoftmax(1)
		self.C = C
		self.biais = 1/C

	def forward(self, utilities_prod_per_nest):
		#utilities_prod_per_nest shape n_index unit nests (entries weighted by membership)
		self.get_mu()
		utilities_mu = utilities_prod_per_nest / self.mu  - self.C * (1 - self.memberships)
		#utilities_mu_exp = utilities_mu.exp()
		#utilities_nest = (utilities_mu_exp + self.biais).sum(1).log()
		utilities_mu_max = utilities_mu.max(1).values
		utilities_mu_exp = (utilities_mu - utilities_mu_max).exp()
		utilities_nest = (utilities_mu_exp + self.biais).sum(1).log() + utilities_mu_max
		
		utilities_nest_mu = torch.mul(utilities_nest, self.mu)
		nest_probas_log = self.logsoftmax(utilities_nest_mu)

		prod_probas_in_nest_log = self.logsoftmax(utilities_mu)
		prod_probas_nest_log = prod_probas_in_nest_log + nest_probas_log.unsqueeze(1).repeat(1,self.num_units,1)
		prod_probas = prod_probas_nest_log.exp().sum(2)   
		return prod_probas

	def get_mu(self):
		self.mu = self.mu_param.sigmoid()
		return self.mu

	def extra_repr(self):
		return 'units={}, nests={}'.format(
			self.num_units, self.num_nests
		)


class CrossingLayer(nn.Module):
	def __init__(self, in_features, units, memberships, C = C, membership_biais = None):
		super(CrossingLayer, self).__init__()
		self.num_units, self.num_nests = memberships.shape
		self.memberships = torch.Tensor(memberships)
		if type(membership_biais) == type(None):
		  self.alpha_param = nn.Parameter(torch.rand(self.memberships.shape) - C * (1 - self.memberships))
		else:
		  self.alpha_param = nn.Parameter(torch.rand(self.memberships.shape) - C * (1 - self.memberships) + torch.Tensor(membership_biais) )
		self.get_alpha()

	def forward(self, utilities):
		#utilities shape n_index unit, return unsqueeze num_nests (entries weighted by membership)
		self.get_alpha()
		utilities_per_nest_effective = utilities.unsqueeze(2).repeat(1,1,self.num_nests) + self.alpha_log
		return utilities_per_nest_effective

	def get_alpha(self):
		self.alpha = self.alpha_param.softmax(1)
		self.alpha_log = self.alpha_param.log_softmax(1)
		return self.alpha

	def extra_repr(self):
		return 'units={}, nests={}'.format(
			self.num_units, self.num_nests
		)


class MNL(nn.Module):
	__constants__ = ['in_features']

	def __init__(self, in_features, units, C = C):
		super(MNL, self).__init__()
		self.linear_layer = LinearLayer(in_features, units, C = C)
		self.softmax = nn.Softmax(1)

	def forward(self, input_tensor):
		utilities = self.linear_layer(input_tensor)
		return self.softmax(utilities)


class NMNL(nn.Module):
	__constants__ = ['in_features']

	def __init__(self, in_features, units, memberships, C = C, linear_layer = None):
		super(NMNL, self).__init__()
		if linear_layer == None:
			self.linear_layer = LinearLayer(in_features, units, C = C)
		else:
			self.linear_layer = linear_layer

		self.nesting_layer = NestingLayer(in_features, units, memberships, C = C)
		self.num_units, self.num_nests = memberships.shape
		self.memberships = torch.Tensor(memberships)


	def forward(self, input_tensor):
		utilities = self.linear_layer(input_tensor)
		utilities_nest = utilities.unsqueeze(2).repeat(1,1,self.num_nests) #* self.memberships
		prod_probas = self.nesting_layer(utilities_nest)
		return prod_probas



class CNL(NMNL):
	__constants__ = ['in_features']

	def __init__(self, in_features, units, memberships, C = C, linear_layer = None, membership_biais = None):
		super(CNL, self).__init__(
			in_features, units, memberships, C = C, linear_layer = linear_layer
		)
		self.crossing_layer = CrossingLayer(in_features, units, memberships, C = C, membership_biais = membership_biais)
		
		
	def forward(self, input_tensor):
		utilities = self.linear_layer(input_tensor)
		prod_probas = self.forward_from_utils(utilities)
		return prod_probas

	def forward_from_utils(self, utilities):
		utilities_per_nest_effective = self.crossing_layer(utilities)
		prod_probas = self.nesting_layer(utilities_per_nest_effective)
		return prod_probas