import pandas as pd
import numpy as np
import random as rd

import mnl, loss, SGD_trainer, global_preprocess, overfit
from global_preprocess import * 
import torch
import torch.optim as optim
import torch.nn as nn
import tqdm
import sklearn.model_selection as skms



N_EPOCHS = int(2e2)
BATCH_SIZE = int(2e2)#100
LR = 1e-1
MAX_PATIENCE = 3
metrics = {
	"mse_loss":		loss.mse_loss , 
	"kl_div":		loss.get_kl_div(), 
	"neg_like":		loss.get_log_like(negative=True),
	"r2_ass":		loss.r2_assortment
}
name_criterion = "neg_like"
sample_size = None#
tqdm_ = False
C = 1e10

LR_overfit = 1.
N_ITER_overfit=100
sample_size_overfit = None





if __name__ == "__main__":
	#Data process
	#--------------------------------------------------------------
	print("Pre-processing data")
	if False:
		str_df = pd.read_csv("data/data_201901_de.csv")
		df_sales_purchase, in_features = global_preprocess.preprocess_data(str_df)
		df_sales = global_preprocess.def_outside_option(df_sales_purchase, train.no_purchase_rate)
	else:
		df_sales = pd.read_csv("data/data_201901_de_preprocessed.csv")
		df_sales.set_index(index, inplace=True)
		
	in_features = list([c for c in df_sales.columns if c not in index +[unit,sales_col] + nest_cols + [col_loading_specific]])
	units = sorted(df_sales[unit].unique())
	df_sales_feat = df_sales[in_features + [unit, sales_col]]
	if sample_size != None: df_sales_feat = df_sales_feat.loc[df_sales.index.unique().values[:sample_size]]
	units_map_index = {k:i for (i,k) in enumerate(units)}
	wslrs_map_index = {k:i for (i,k) in enumerate(df_sales_feat.index.unique())}
	#df_sales_feat_all_units = df_sales_feat.groupby(index).apply(lambda df: df.set_index(unit).reindex(units))


	print("Tensoring data")
	df_sales_tensor = np.zeros((df_sales_feat.index.unique().shape[0], len(units), len(in_features) + 2))
	for (k,row) in df_sales_feat.set_index(unit, append=True).iterrows():
		w,u = wslrs_map_index[k[:2]], units_map_index[k[2]]
		df_sales_tensor[w, u, 0 ] = 1.
		df_sales_tensor[w, u, 1:] = row.values

	df_sales_tensor = torch.Tensor(df_sales_tensor)
	sales_col_tensor = df_sales_tensor[:,:, -1]
	df_sales_tensor  = df_sales_tensor[:,:,:-1]

	memberships = pd.get_dummies(df_sales.groupby(unit)[nest_cols[0]].first()).values
	memberships_cnl = pd.get_dummies(df_sales.groupby(unit)[nest_cols].first()).drop('WAMP_v_CALC_0',1).values
	memberships_all = np.zeros_like(memberships); memberships_all[0,0] = 1; memberships_all[1:,1:] = 1


	#Train models
	#---------------------------------------------------------------------
	indices = list(range(df_sales_tensor.shape[0]))
	split_val = .3
	training_indices, testing_indices = skms.train_test_split(indices, test_size=0.5, random_state=1)
	training_indices, validation_indices = skms.train_test_split(training_indices, test_size=0.3, random_state=1)

	for model_type in ["MNL","CNL",]:# "NMNL",  "CNL_all"] :

		if model_type=="MNL":
			model = mnl.MNL(in_features=in_features, units=units)
		elif model_type=="NMNL":	
			model = mnl.NMNL(
				in_features, units, memberships, 
				linear_layer = model.linear_layer
			)
		elif model_type=="CNL":	
			model = mnl.CNL(
				in_features, units, memberships_cnl, 
				linear_layer = model.linear_layer
			)
		elif model_type == "CNL_all":
			model = mnl.CNL(
				in_features, units, memberships_all, 
				linear_layer = model.linear_layer, membership_biais = memberships
			)


		optimizer = optim.Adagrad(model.parameters(), lr=LR)
		criterion = metrics[name_criterion]
		trainer = SGD_trainer.SGD_trainer(
			model, criterion, optimizer, 
			df_sales_tensor, sales_col_tensor,
			training_indices, validation_indices, max_patience = MAX_PATIENCE
		)

		print("\n ======================================================================== ")
		print("\n", model_type, ' model\t-\t', name_criterion.upper(), ' loss\t-\tStart Training')
		trainer.train(BATCH_SIZE, N_EPOCHS, tqdm_ = tqdm_)
		print("\n", model_type, ' model\t-\t', name_criterion.upper(), ' loss\t-\tFinished Training \n')


		y_hat_train = model(df_sales_tensor[training_indices])
		y_train = sales_col_tensor[training_indices]
		y_hat_test = model(df_sales_tensor[testing_indices])
		y_test = sales_col_tensor[testing_indices]
		
		for (name, metrix) in metrics.items():
			train_m, base_m = loss.get_metric(model, y_hat_train, y_train, metrix)
			test_m , ______ = loss.get_metric(model, y_hat_test , y_test , metrix)
			print(name, ":\t\t train : %.6e \t test  : %.6e \t base: %.3e"%(train_m, test_m, base_m)  )

		torch.save(model, "models/%s.pt"%model_type)



	#Save parametric utility
	#----------------------------------------------------------------------------------
	df_parametric = pd.DataFrame(
	  model.linear_layer(df_sales_tensor[:None,:,:]).detach().numpy(),columns = units
	)
	df_parametric[wslr_col] = [w[0] for w in list(wslrs_map_index.keys())[:sample_size_overfit]]
	df_parametric[yrmo_col] = [w[1] for w in list(wslrs_map_index.keys())[:sample_size_overfit]]
	df_parametric = df_parametric.melt(id_vars=df_parametric.columns[-2:], value_vars=df_parametric.columns[:-2])
	df_parametric.columns = ['WSLR_CUST_PARTY_ID','cal_yr_mo_nbr', 'SHLF_PROD_ID','BETA_parametric']
	df_parametric = df_parametric.loc[df_parametric['BETA_parametric'] > -1e2]
	df_parametric.astype(float).to_pickle('models/CNL_parametric_util.pkl')



	#Overfit model
	#------------------------------------------------------------------------------------
	print("\n ======================================================================== \n" + "\n Overfitting model \n")
	#model = torch.load("models/CNL.pt")
	cnl_model = mnl.CNL([], units, memberships_cnl)
	cnl_model.nesting_layer.mu_param = nn.Parameter(model.nesting_layer.mu_param.detach())
	cnl_model.crossing_layer.alpha_param = nn.Parameter(model.crossing_layer.alpha_param.detach())
	cnl_model.linear_layer.beta_dummy = nn.Parameter(model.linear_layer.beta_dummy.detach())
	#cnl_model.linear_layer.beta_feat = nn.Parameter(model.linear_layer.beta_feat.detach())

	df_overfit = df_sales[[unit, sales_col]].loc[list(wslrs_map_index.keys())[:sample_size_overfit]]\
	.groupby(index).apply(
	    overfit.overfit_cnl, 
	    cnl_model = cnl_model, units_map_index=units_map_index,
	    n_iter = N_ITER_overfit, lr = LR_overfit
	)

	df_overfit_wide = df_overfit.reset_index(level = [0,1]).pivot_table(index = index, columns=unit, values='BETA', fill_value=-C)
	df_overfit_wide.to_pickle("models/CNL_overfit_betas.pkl")


	#validate overfit
	for u in units: 
		if u not in df_overfit_wide.columns: df_overfit_wide[u] = -C
	y_overfit = cnl_model.forward_from_utils(torch.Tensor(df_overfit_wide[units].values) + C * (df_sales_tensor[:sample_size_overfit,:,0] - 1))
	for (name, metrix) in metrics.items():
		train_m, base_m = loss.get_metric(model, y_overfit, sales_col_tensor[:sample_size_overfit], metrix)
		print(name, ":\t\t Overfit loss: %.6e \t base: %.3e"%(train_m, base_m)  )

	