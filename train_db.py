import pandas as pd
import numpy as np
import random as rd

import mnl, loss
import torch
import torch.optim as optim
import tqdm



N_EPOCHS = int(2e1)
BATCH_SIZE = int(2e2)#100
LR = 1e-1
metrics = {
	"mse_loss":		mse_loss , 
	"kl_div":		get_kl_div(), 
	"neg_log_like":	get_log_like(negative=True)
}
name_criterion = "neg_log_like"
sample_size = None#1000


features_base = ["price", "OZ_PER_UNIT", "ALCOHOL", ]# 'Alcohol_by_Volume_Value']
features_store = ['LONG', 'LATT', 'MEDIAN_INCOME', 'PERC_BLACK', 'PERC_HISP','POP_DENSITY']
features_OH = ['SHLF_PROD_ID'] #+ ['STYLES_TO_USE', 'WAMP_v_CALC']
features_OH_S = ['CHANNEL', 'URBANICITY']

wslr_col = "WSLR_CUST_PARTY_ID"
yrmo_col = "cal_yr_mo_nbr" 
sales_col = "DLVRD_CASE_EQV_QTY"
unit = 'SHLF_PROD_ID'
index = [wslr_col, yrmo_col]
nest_cols = ['STYLES_TO_USE', 'WAMP_v_CALC']
col_loading_specific = 'STYLESxWAMP'

no_purchase_rate = .05




def preprocess_data(str_df):
	"""DATA PREPROCESS"""
	df_sales = str_df

	#remove duplicate units in sales
	df_sales = df_sales.drop_duplicates([wslr_col, yrmo_col, unit], keep='first').drop(sales_col,1)\
	.merge(
      df_sales.groupby([wslr_col, yrmo_col, unit], as_index = False)[sales_col].sum(), 
      on = [wslr_col, yrmo_col, unit]
    )

	#1 HOT ENCODING - Nest included
	df_sales[sales_col] = df_sales[sales_col].astype(float)
	df_sales = pd.concat([df_sales] + 
	  [pd.get_dummies(df_sales[f],prefix=f,drop_first=False) 
	   for f in features_OH + features_OH_S 
	  ], axis=1
	)


	#ENGINEER FEATURES and U-specific features
	df_sales['price_per_unit'] = df_sales['price'] * df_sales['OZ_PER_UNIT']
	df_sales['price_log'] = np.log(df_sales['price'])
	df_sales['OZ_log'] = np.log(df_sales['OZ_PER_UNIT'])
	features_prod = features_base + ['price_per_unit', 'price_log', 'OZ_log']
	df_sales = pd.concat([df_sales] + [
	  pd.get_dummies(df_sales[col_loading_specific], prefix=f).mul(df_sales[f],0)
	  for f in features_prod
	], axis=1)

	feat_prod_spec = np.concatenate([[f + '_' + str(n) for f in features_prod] for n in df_sales[col_loading_specific].unique()]).tolist()
	features_num = features_store + feat_prod_spec
	feat_prod_OH  = np.concatenate([[f for f in df_sales.columns if f.startswith(foh + '_')] for foh in features_OH]).tolist()
	feat_prod_OH_S= np.concatenate([[f for f in df_sales.columns if f.startswith(foh + '_')] for foh in features_OH_S]).tolist()
	features_all   = features_num + feat_prod_OH + feat_prod_OH_S

	#SCALE FEATURES AND REINDEX
	df_sales[features_num] = df_sales[features_num].astype(float)
	df_sales_features_scale = df_sales[features_num].abs().max(0)
	df_sales[features_num] = df_sales[features_num].div(df_sales_features_scale)


	in_features = features_num + feat_prod_OH_S
	return df_sales, in_features


def def_outside_option(df_sales_purchase, no_purchase_rate = .05):
	df_sales = df_sales_purchase.copy()
	df_sales.set_index([wslr_col, yrmo_col], inplace=True) 
	df_nopurchase = df_sales.groupby(level = [wslr_col, yrmo_col]).agg({sales_col:'sum'}) * no_purchase_rate / (1 - no_purchase_rate)
	df_sales = pd.concat([
		df_sales, df_nopurchase
	], sort=True).fillna(0, downcast='infer')
  
	#NORAMLIZE TO MS
	df_sales[sales_col] /= df_sales.groupby(level = [wslr_col, yrmo_col])[sales_col].transform('sum')
	df_sales.sort_index(inplace=True)
	return df_sales


def desensitize_data(str_df):
	mapping_wslr = {k:i+100 for (i,k) in enumerate(str_df[wslr_col].unique())}
	mapping_unit = {k:i+100 for (i,k) in enumerate(str_df[unit].unique())}
	mapping_nests = {
	    'CRAFT & STYLES':'STYLE_1',
	    'EASY DRINKING':'STYLE_2',
	    'BALANCED CHOICES':'STYLE_3',
	    'CLASSIC LAGER':'STYLE_4',
	    'BEYOND BEER':'STYLE_5',
	    
	    'SUPER PREMIUM':'WAMP_1',
	    'CORE':'WAMP_2',
	    'CORE PLUS':'WAMP_3',
	    'PREMIUM':'WAMP_4',
	    'VALUE':'WAMP_5',
	    
	    'CRAFT & STYLES/SUPER PREMIUM':'SW_1',
	    'EASY DRINKING/CORE':'SW_2',
	    'BALANCED CHOICES/CORE PLUS':'SW_3',
	    'CLASSIC LAGER/CORE':'SW_4',
	    'CLASSIC LAGER/PREMIUM':'SW_5',
	    'EASY DRINKING/CORE PLUS':'SW_6',
	    'BEYOND BEER/PREMIUM':'SW_7',
	    'CLASSIC LAGER/SUPER PREMIUM':'SW_8',
	    'CLASSIC LAGER/VALUE':'SW_9',
	    'EASY DRINKING/PREMIUM':'SW_10',
	    'EASY DRINKING/SUPER PREMIUM':'SW_11',
	    'CRAFT & STYLES/PREMIUM':'SW_12',
	    'BEYOND BEER/SUPER PREMIUM':'SW_13',
	    'EASY DRINKING/VALUE':'SW_14',
	    'CLASSIC LAGER/CORE PLUS':'SW_15',
	    'BALANCED CHOICES/PREMIUM':'SW_16',
	    'BEYOND BEER/CORE':'SW_17',
	    'BEYOND BEER/CORE PLUS':'SW_18',
	}


def train_model(model, criterion, optimizer, 
	df_sales_tensor, sales_col_tensor,
	training_indices, BATCH_SIZE, N_EPOCHS,
	):
	pbar = tqdm.trange(N_EPOCHS)
	for epoch in pbar:  # loop over the dataset multiple times
	    running_loss = 0.0
	    rd.shuffle(training_indices)

	    for i in range(len(training_indices)//BATCH_SIZE):
	        a = training_indices[i*BATCH_SIZE:(i+1)*BATCH_SIZE]
	        x = df_sales_tensor[a]
	        y = sales_col_tensor[a]

	        
	        # forward + backward + optimize
	        optimizer.zero_grad()
	        y_hat = model(x)
	        cur_loss = criterion(model, y_hat, y)
	        cur_loss.backward()
	        optimizer.step()
	        running_loss += cur_loss.item()
	    running_loss /= (len(training_indices)//BATCH_SIZE)
	    pbar.set_description("Loss: %.3e" % running_loss) 
	return model	


if __name__ == "__main__":
	#str_df = pd.read_csv("data/data201901.csv")
	#model = torch.load("models/MNL.pt")
	str_df = pd.read_csv("data/data_201901_de.csv")

	#Data process
	#--------------------------------------------------------------
	print("Pre-processing data")
	df_sales_purchase, in_features = preprocess_data(str_df)
	df_sales = def_outside_option(df_sales_purchase, no_purchase_rate)
	units = sorted(df_sales[unit].unique())
	df_sales_feat = df_sales[in_features + [unit, sales_col]]
	if sample_size != None: df_sales_feat = df_sales_feat.loc[df_sales.index.unique().values[:sample_size]]
	units_map_index = {k:i for (i,k) in enumerate(units)}
	wslrs_map_index = {k:i for (i,k) in enumerate(df_sales_feat.index.unique())}


	print("Tensoring data")
	df_sales_tensor = np.zeros((df_sales_feat.index.unique().shape[0], len(units), len(in_features) + 2))
	for (k,row) in df_sales_feat.set_index(unit, append=True).iterrows():
	    w,u = wslrs_map_index[k[:2]], units_map_index[k[2]]
	    df_sales_tensor[w, u, 0 ] = 1.
	    df_sales_tensor[w, u, 1:] = row.values

	df_sales_tensor = torch.Tensor(df_sales_tensor)
	sales_col_tensor = df_sales_tensor[:,:,-1]
	df_sales_tensor  = df_sales_tensor[:,:,:-1]

	memberships = pd.get_dummies(df_sales.groupby(unit)[nest_cols[0]].first()).values
	memberships_cnl = pd.get_dummies(df_sales.groupby(unit)[nest_cols].first()).drop('WAMP_v_CALC_0',1).values
	memberships_all = np.zeros_like(memberships); memberships_all[0,0] = 1; memberships_all[1:,1:] = 1


	#Train models
	#---------------------------------------------------------------------
	training_indices = list(range(df_sales_tensor.shape[0]))

	for model_type in  ["MNL", "NMNL", "CNL", "CNL_all"] : #["CNL_all"]:#

		if model_type=="MNL":
			model = MNL(in_features=in_features, units=units)
		elif model_type=="NMNL":	
			model = NMNL(
				in_features, units, memberships, 
				linear_layer = model.linear_layer
			)
		elif model_type=="CNL":	
			model = CNL(
				in_features, units, memberships_cnl, 
				linear_layer = model.linear_layer
			)
		elif model_type == "CNL_all":
			model = CNL(
				in_features, units, memberships_all, 
				linear_layer = model.linear_layer, membership_biais = memberships
			)


		optimizer = optim.Adagrad(model.parameters(), lr=LR)
		criterion = metrics[name_criterion]

		print("\n ======================================================================== ")
		print("\n", model_type, ' model\t-\t', name_criterion.upper(), ' loss\t-\tStart Training \n')
		model = train_model(model, criterion, optimizer, 
			df_sales_tensor, sales_col_tensor,
			training_indices, BATCH_SIZE, N_EPOCHS,
		)
		print("\n", model_type, ' model\t-\t', name_criterion.upper(), ' loss\t-\tFinished Training \n')


		y_hat = model(df_sales_tensor)
		y = sales_col_tensor
		baseline = y.mean(0).repeat((y.shape[0],1))
		
		for (name, metrix) in metrics.items():
			train_m, test_m = get_metric(model, y_hat, y, metrix, baseline)
			print(name, ":\t\t model : %.8e \t base: %.3e"%(train_m, test_m)  )

		torch.save(model, "models/%s.pt"%model_type)
