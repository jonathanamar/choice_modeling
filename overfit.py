import loss, mnl, SGD_trainer
import torch.nn as nn
import torch.optim as optim
import torch
import pandas as pd
from global_preprocess import *

n_iter = 100
lr=1.


"Pyspark code"
if False:
	schema_str = StructType([
		StructField(wslr_col,DoubleType(),True), StructField(yrmo_col,DoubleType(),True),
		StructField(unit,DoubleType(),True), StructField('BETA', DoubleType(), True)
	])
	@pandas_udf(schema_str, PandasUDFType.GROUPED_MAP)
	def overfit_cnl(df_sales_sample):
		wslr = df_sales_sample[wslr_col].iloc[0]  
		yrmo = df_sales_sample[yrmo_col].iloc[0]  
			
		overfiter = overfit_cnl_class(cnl_model, unit=unit, sales_col=sales_col, units_map_index = units_map_index)
		overfiter.fit(df_sales_sample[[unit,sales_col]], n_iter, lr = lr)
		
		dummies = overfiter.dummies
		dummies[wslr_col] = wslr
		dummies[yrmo_col] = yrmo
		return dummies

else:		
	def overfit_cnl(df_sales_sample, **kwargs):
		overfiter = overfit_cnl_class(cnl_model = kwargs['cnl_model'], units_map_index = kwargs['units_map_index'])
		overfiter.fit(df_sales_sample[[unit,sales_col]], n_iter = kwargs['n_iter'], lr = kwargs['lr'])
		dummies = overfiter.dummies
		return dummies



class overfit_cnl_class():
	def __init__(self, cnl_model, units_map_index,):
		self.cnl_model = cnl_model
		self.units_map_index = units_map_index
		self.sales_col = sales_col
		self.unit = unit
		self.memberships_cnl = self.cnl_model.memberships

	
	def initialize_cnl_overfit(self):
		self.cnl_overfit = mnl.CNL([], self.units_sample, self.memberships_cnl[self.units_sample_index])
		self.cnl_overfit.crossing_layer.alpha_param = nn.Parameter(self.cnl_model.crossing_layer.state_dict()['alpha_param'][self.units_sample_index])
		self.cnl_overfit.crossing_layer.requires_grad_(False)
		self.cnl_overfit.linear_layer.beta_feat.requires_grad_(False)
		self.cnl_overfit.nesting_layer.load_state_dict(self.cnl_model.nesting_layer.state_dict())
		self.cnl_overfit.nesting_layer.mu_param.requires_grad_(False)

	
	def fit(self, df_sales_sample, n_iter, lr = 1.):	
		df_sales_sample = df_sales_sample.sort_values(self.unit)
		self.units_sample = sorted(df_sales_sample[self.unit].values)
		self.units_sample_index = [self.units_map_index[u] for u in self.units_sample]
		df_sales_tensor_sample = torch.ones((1, len(self.units_sample), 1))
		sales_col_tensor_sample = torch.Tensor(df_sales_sample[self.sales_col].values).unsqueeze(0)
		
		self.initialize_cnl_overfit()
		optimizer = optim.Adam(self.cnl_overfit.parameters(), lr=lr)
		criterion = loss.get_log_like(negative=True)
		trainer = SGD_trainer.Overfit_trainer(
			self.cnl_overfit, criterion, optimizer, 
			df_sales_tensor_sample, sales_col_tensor_sample,
		)
		trainer.fit_model(n_iter)
		
		self.dummies = self.cnl_overfit.linear_layer.dummies.detach().numpy()[:,0]
		self.dummies = pd.Series({u: self.dummies[i] for (i,u) in enumerate(self.units_sample)}).to_frame().reset_index()
		self.dummies.columns = [self.unit,'BETA']
		