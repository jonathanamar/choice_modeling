# Choice Modeling
General pipeline using pytorch to train model using aggregate statistics at different sale/time indices.

Models supported:
- MNL
- Nested MNL
- Cross-nested MNL

The overfit process is to serve as input to a imputation of wholesales.
Final model is parametric utility model + low rank correction (not covered here).

The revenue function feeds into an optimization procedure, balancing for revenue and market share.
