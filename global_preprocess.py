import pandas as pd
import numpy as np


features_base = ["price", "OZ_PER_UNIT", "ALCOHOL", ]# 'Alcohol_by_Volume_Value']
features_store = ['LONG', 'LATT', 'MEDIAN_INCOME', 'PERC_BLACK', 'PERC_HISP','POP_DENSITY']
features_OH =  [] #['SHLF_PROD_ID'] #+ ['STYLES_TO_USE', 'WAMP_v_CALC']
features_OH_S = ['CHANNEL', 'URBANICITY']

wslr_col = "WSLR_CUST_PARTY_ID"
yrmo_col = "cal_yr_mo_nbr" 
sales_col = "DLVRD_CASE_EQV_QTY"
unit = 'SHLF_PROD_ID'
index = [wslr_col, yrmo_col]
nest_cols = ['STYLES_TO_USE', 'WAMP_v_CALC']
col_loading_specific = 'STYLESxWAMP'

no_purchase_rate = .05




def preprocess_data(str_df):
	"""DATA PREPROCESS"""
	df_sales = str_df

	#remove duplicate units in sales
	df_sales = df_sales.drop_duplicates([wslr_col, yrmo_col, unit], keep='first').drop(sales_col,1)\
	.merge(
      df_sales.groupby([wslr_col, yrmo_col, unit], as_index = False)[sales_col].sum(), 
      on = [wslr_col, yrmo_col, unit]
    )

	#1 HOT ENCODING
	df_sales[sales_col] = df_sales[sales_col].astype(float)
	df_sales = pd.concat([df_sales] + 
	  [pd.get_dummies(df_sales[f], prefix=f, drop_first=True) for f in features_OH] +
	  [pd.get_dummies(df_sales[f], prefix=f, drop_first=False) for f in features_OH_S ]
	  , axis=1
	)


	#ENGINEER FEATURES and U-specific features
	added_feat = []
	for f in features_store:
      df_sales[f+'_sq'] = df_sales[f]*df_sales[f]; added_feat.append(f+'_sq')
	#df_sales['price_per_unit'] = df_sales['price'] * df_sales['OZ_PER_UNIT']; added_feat.append('price_per_unit')
	#df_sales['price_log'] = np.log(df_sales['price']); added_feat.append('price_log')
	#df_sales['OZ_log'] = np.log(df_sales['OZ_PER_UNIT']); added_feat.append('OZ_log')
	features_prod = features_base + added_feat
	df_sales = pd.concat([df_sales] + [
	  pd.get_dummies(df_sales[col_loading_specific], prefix=f).mul(df_sales[f],0)
	  for f in features_prod
	], axis=1)

	feat_prod_spec = np.concatenate([[f + '_' + str(n) for f in features_prod] for n in df_sales[col_loading_specific].unique()]).tolist()
	features_num = features_store + feat_prod_spec
	feat_prod_OH  = \
		[] if features_OH == [] else \
		np.concatenate([[f for f in df_sales.columns if f.startswith(foh + '_')] for foh in features_OH]).tolist()
	feat_prod_OH_S= \
		[] if features_OH_S == [] else \
		np.concatenate([[f for f in df_sales.columns if f.startswith(foh + '_')] for foh in features_OH_S]).tolist()
	features_all   = features_num + feat_prod_OH + feat_prod_OH_S

	#SCALE FEATURES AND REINDEX
	df_sales[features_num] = df_sales[features_num].astype(float)
	df_sales_features_scale = df_sales[features_num].abs().max(0)
	df_sales[features_num] = df_sales[features_num].div(df_sales_features_scale)
	#df_sales_features_scale_mean = df_sales[features_num].mean(0); df_sales_features_scale_std = df_sales[features_num].std(0); 
	#df_sales[features_num] = (df_sales[features_num] - df_sales_features_scale_mean) / df_sales_features_scale_std

	in_features = features_num + feat_prod_OH_S
	return df_sales[index + [unit, sales_col, col_loading_specific] + nest_cols + in_features], in_features


def def_outside_option(df_sales_purchase, no_purchase_rate = None):
	df_sales = df_sales_purchase.copy(); 
	df_sales = df_sales.loc[:,~df_sales.columns.duplicated()]
	if no_purchase_rate: df_sales['npr'] = no_purchase_rate

	df_sales.set_index([wslr_col, yrmo_col], inplace=True) 
	df_nopurchase = df_sales.groupby(level = [wslr_col, yrmo_col]).agg({sales_col:'sum', 'npr':'first'})
	df_nopurchase[sales_col] = df_nopurchase[sales_col] * df_nopurchase['npr'] / (1 - df_nopurchase['npr'])
	df_sales = pd.concat([
		df_sales, df_nopurchase
	], sort=True).fillna(0, downcast='infer')
  
	#NORAMLIZE TO MS
	df_sales[sales_col] /= df_sales.groupby(level = [wslr_col, yrmo_col])[sales_col].transform('sum')
	df_sales.sort_index(inplace=True)
	return df_sales


def desensitize_data(str_df):
	mapping_wslr = {k:i+100 for (i,k) in enumerate(str_df[wslr_col].unique())}
	mapping_unit = {k:i+100 for (i,k) in enumerate(str_df[unit].unique())}
	mapping_nests = {
	    'CRAFT & STYLES':'STYLE_1',
	    'EASY DRINKING':'STYLE_2',
	    'BALANCED CHOICES':'STYLE_3',
	    'CLASSIC LAGER':'STYLE_4',
	    'BEYOND BEER':'STYLE_5',
	    
	    'SUPER PREMIUM':'WAMP_1',
	    'CORE':'WAMP_2',
	    'CORE PLUS':'WAMP_3',
	    'PREMIUM':'WAMP_4',
	    'VALUE':'WAMP_5',
	    
	    'CRAFT & STYLES/SUPER PREMIUM':'SW_1',
	    'EASY DRINKING/CORE':'SW_2',
	    'BALANCED CHOICES/CORE PLUS':'SW_3',
	    'CLASSIC LAGER/CORE':'SW_4',
	    'CLASSIC LAGER/PREMIUM':'SW_5',
	    'EASY DRINKING/CORE PLUS':'SW_6',
	    'BEYOND BEER/PREMIUM':'SW_7',
	    'CLASSIC LAGER/SUPER PREMIUM':'SW_8',
	    'CLASSIC LAGER/VALUE':'SW_9',
	    'EASY DRINKING/PREMIUM':'SW_10',
	    'EASY DRINKING/SUPER PREMIUM':'SW_11',
	    'CRAFT & STYLES/PREMIUM':'SW_12',
	    'BEYOND BEER/SUPER PREMIUM':'SW_13',
	    'EASY DRINKING/VALUE':'SW_14',
	    'CLASSIC LAGER/CORE PLUS':'SW_15',
	    'BALANCED CHOICES/PREMIUM':'SW_16',
	    'BEYOND BEER/CORE':'SW_17',
	    'BEYOND BEER/CORE PLUS':'SW_18',
	}



--------------------------------------------------------------------------------------------------------------------
"""DATA PREPROCESS and feature generation"""
def preprocess_data(str_df):
	"""DATA PREPROCESS"""
	df_sales = str_df

	#remove duplicate units in sales
	df_sales = df_sales.drop_duplicates([wslr_col, yrmo_col, unit], keep='first').drop(sales_col,1)\
	.merge(
      df_sales.groupby([wslr_col, yrmo_col, unit], as_index = False)[sales_col].sum(), 
      on = [wslr_col, yrmo_col, unit]
    )

	#1 HOT ENCODING - Nest included
	df_sales[sales_col] = df_sales[sales_col].astype(float)
	df_sales = pd.concat([df_sales] + 
	  [pd.get_dummies(df_sales[f],prefix=f,drop_first=True) 
	   for f in features_OH + features_OH_S 
	  ], axis=1
	)


	#ENGINEER FEATURES and U-specific features for product specific features
	added_feat = []; 
	features_prod = features_base + added_feat
	df_sales = pd.concat([df_sales] + [
	  pd.get_dummies(df_sales[col_loading_specific], prefix=f).mul(df_sales[f],0)
	  for f in features_prod
	], axis=1)
	
	
	square_features_store=features_to_square
	for f in features_to_square: 
		df_sales[f+'_sq'] = df_sales[f]*df_sales[f]; 
		square_features_store.append(f+'_sq')
	

	feat_prod_spec = np.concatenate([[f + '_' + str(n) for f in features_prod] for n in df_sales[col_loading_specific].unique()]).tolist()
	features_num = features_store + feat_prod_spec + square_features_store
	feat_prod_OH  = [] if features_OH == [] else \
		np.concatenate([[f for f in df_sales.columns if f.startswith(foh + '_')] for foh in features_OH]).tolist()
	feat_prod_OH_S= [] if features_OH_S == [] else \
		np.concatenate([[f for f in df_sales.columns if f.startswith(foh + '_')] for foh in features_OH_S]).tolist()
	features_all   = features_num + feat_prod_OH + feat_prod_OH_S

	#SCALE FEATURES AND REINDEX
	df_sales[features_num] = df_sales[features_num].astype(float)
	df_sales_features_scale = df_sales[features_num].abs().max(0)
	df_sales[features_num] = df_sales[features_num].div(df_sales_features_scale)
	#df_sales_features_scale_mean = df_sales[features_num].mean(0); df_sales_features_scale_std = df_sales[features_num].std(0); 
	#df_sales[features_num] = (df_sales[features_num] - df_sales_features_scale_mean) / df_sales_features_scale_std

	in_features = features_num + feat_prod_OH_S
	return df_sales[index + [unit, sales_col, col_loading_specific] + nest_cols + in_features], in_features, df_sales_features_scale