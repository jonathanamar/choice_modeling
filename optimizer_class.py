print("""
Optimization package for Kroger data
Use LP solver for optimization
Product constraints are unit specific for competition products

In this version:
we optimize for ABI revenue
keep competition fixed
only expand on possible ABI products
dual-path is still keeping ABI m-s improved
""")


### inputs --> data, cnl model, variables, table name
### outputs --> save df of assortments


## data (not init) --> df_spark

## cnl model (init) --> cnl_model

## variables (init) --> units_map_index,
##           --> t, max_iter, size_block, n_restarts, percent_threshold, min_samples
##           --> price_transformation
##           --> n_restart_per_bissection, n_bissection, market_share_threshold

## table name (not init) --> save_name



index_nz = 0 #Set to 1 if outside option counted separately

class FW_optimizer():
    def __init__(self, cnl_model,
                 units_map_index, memberships_cnl,
                 max_iter=25, size_block=100, min_samples=2, t=200,
                 price_transformation=lambda x: x,
                 robust_param=1,
                 n_restart_per_bissection=10, n_bissection=5,
                 schema_str = None,
                **kwargs):
        self.cnl_model = cnl_model
        self.units_map_index = units_map_index
        self.memberships_cnl = memberships_cnl

        self.max_iter = max_iter
        self.size_block = size_block
        self.min_samples = min_samples
        self.price_transformation = price_transformation
        self.robust_param = robust_param
        self.n_restart_per_bissection = n_restart_per_bissection
        self.n_bissection = n_bissection
        self.t = t
        self.set_schema(schema_str)
        
        
    def set_schema(self, schema_str):
        ## These are the column names and column types for the raw output from the optimizer
        if schema_str is not None:
          self.schema_str = schema_str
        else:
          self.schema_str = StructType([
            StructField(wslr_col, StringType(), True), StructField(yrmo_col, LongType(), True),
            StructField('vpb', DoubleType(), True), StructField('npr', DoubleType(), True),
            StructField('L', DoubleType(), True), StructField('L_p', DoubleType(), True),
            StructField('ms_threshold', DoubleType(), True), StructField('expand', DoubleType(), True),
            StructField('seed', DoubleType(), True),
            StructField(unit, ArrayType(DoubleType()), True),
            StructField('pred_revenue', DoubleType(), True), StructField('pred_ms', DoubleType(), True),
            StructField('revenue_fin_ab', DoubleType(), True), StructField('markets_fin_ab', DoubleType(), True),
            StructField('revenue_fin_rt', DoubleType(), True), StructField('markets_fin_rt', DoubleType(), True),

        ])

    
    def define_append_results(self, wslr, yrmo, npr = 0.):
        ## create a function to append results to the main results dataframe
        def append_results(
            results, vpb_cur, L, L_p, market_share_threshold, expand, seed, 
            markets_fin, revenue_fin, s_final,
            markets_fin_ab, revenue_fin_ab, markets_fin_rt, revenue_fin_rt
            ):
            d = pd.DataFrame({
                wslr_col: [wslr], yrmo_col: [yrmo],
                'vpb': vpb_cur, 'npr': [npr], 'L': [L], 'L_p': [L_p], 'ms_threshold': [market_share_threshold],
                'expand': [expand],
                'seed': [seed], 'pred_revenue': [revenue_fin], 'pred_ms': [markets_fin],
                unit: [np.array(s_final)],
                'markets_fin_ab':markets_fin_ab, 'revenue_fin_ab':revenue_fin_ab, 
                'markets_fin_rt':markets_fin_rt, 'revenue_fin_rt':revenue_fin_rt
            })
            results = results.append(d)
            return results
        return append_results
  
    
    def data_to_tensor(self, df):
        # from df we extract the values of the utilities, penalties, prices, presence 
        # for different products and convert them to torch tensors. Size of each tensor is 1*n_products
        df.sort_values(unit, inplace=True)
        util_sample_u = torch.Tensor(df['UTIL'].values).unsqueeze(0)
        penal_sample = torch.Tensor(df['PENAL'].values).unsqueeze(0)
        util_sample = util_sample_u - self.robust_param * penal_sample  ## only has the rows corresponding to the specific products
        price_sample = torch.Tensor(df['PRICE'].values).unsqueeze(0)
        presence_sample = torch.Tensor(df['PRES'].values).unsqueeze(0)
        units = sorted(df[unit].unique())

        return df, util_sample_u, util_sample, penal_sample, price_sample, presence_sample
    
    
    def filter_products(self, df, util_sample_u, util_sample, penal_sample, price_sample, presence_sample):
        #Filter out products that will never be expanded on
        current_assortment = presence_sample[0].numpy()
        current_assortment = [units[i] for (i,a) in enumerate(current_assortment) if a == 1.]
        
        expandable = util_sample_u[0].numpy()
        expandable = [units[i] for (i,a) in enumerate(expandable) if a > -1e2]
        
        try:
            units_sample_allowed = sorted(
              np.union1d(current_assortment, 
                         np.intersect1d(expandable, units_ab_from_ab)
              ).tolist() )
        except:
            raise ValueError("Need to define 'units_ab_from_ab', i.e. list of ab products which will be served for expansion")
        units_map_index_allowed = {u:i for (i,u) in enumerate(units_sample_allowed)}     

        ## create allowed tensors, by selecting only the allowed columns from all tensors
        units_sample_index_allowed = [units_map_index[u] for u in units_sample_allowed]
        util_sample_allowed = util_sample[:, units_sample_index_allowed]
        price_sample_allowed = price_sample[:, units_sample_index_allowed]
        presence_sample_allowed = presence_sample[:, units_sample_index_allowed]
        
        return units_sample_allowed, units_map_index_allowed, units_sample_index_allowed, \
            util_sample_allowed, price_sample_allowed, presence_sample_allowed



    def create_cnl_object(self, units_sample_allowed, units_sample_index_allowed):
        #Creates the CNL object on the subsample of products
        cnl_allowed = mnl.CNL([], units_sample_allowed, self.memberships_cnl[units_sample_index_allowed])
        cnl_allowed.crossing_layer.alpha_param = nn.Parameter(self.cnl_model.crossing_layer.state_dict()\
            ['alpha_param'][units_sample_index_allowed])
        cnl_allowed.nesting_layer.load_state_dict(self.cnl_model.nesting_layer.state_dict())
        cnl_allowed.requires_grad_(False)
        return cnl_allowed


    def define_competition_masks(self, price_sample_allowed, units_sample_allowed, units_map_index_allowed, 
        units_ab_from_ab):
        #Define the competition mask for retailer and for ab
        competition_mask = torch.ones_like(price_sample_allowed)
        competition_mask_0 = torch.ones_like(price_sample_allowed)
        competition_mask[:, [units_map_index_allowed[i] for i in units_sample_allowed if i not in units_ab_from_ab]] = 0.
        competition_mask_0[:, [units_map_index_allowed[i] for i in units_sample_allowed if i in [0]]] = 0.
        return competition_mask,competition_mask_0


    def generate_matrix_constraints(self, RF, L_p, competition_mask, *args):
        #Generate the matrix constraints for the problem we want to solve
        current_assortment_not_ab = ((competition_mask == 0) * (RF.presence_tensor)).numpy()[:,RF.index_nz:]
        
        ## Calculate the absoulte flexibility from the relative flexibility.
        L = np.max([int((len(RF.assortment_cur) - current_assortment_not_ab.sum(1)) * L_p), 3])
        print("CONSTRAINTS:\t", L, len(RF.assortment_cur), current_assortment_not_ab.sum(1))
        
        ## Generate the A,b matrices in Ax<b using the get_matrix_cstr function in the RF object  
        A, b = RF.get_matrix_cstr(L)[:2]
        A = np.concatenate([A, -current_assortment_not_ab],0)
        b = np.concatenate([b, -current_assortment_not_ab.sum(1)]).astype(int)
        return A,b,L

    def generate_block_elements(self , RF, units_map_index_allowed, units_ab_from_ab):
        return [i-RF.index_nz for (u,i) in units_map_index_allowed.items() if u in units_ab_from_ab]

    def get_objectives(self, RF, assortment, competition_mask, competition_mask_0):
        revenue_cur_ab = RF.pred_revenue(assortment, objective='rv', competition_mask = competition_mask)
        markets_cur_ab = RF.pred_revenue(assortment, objective='ms', competition_mask = competition_mask)
        revenue_cur_rt = RF.pred_revenue(assortment, objective='rv', competition_mask = competition_mask_0)
        markets_cur_rt = RF.pred_revenue(assortment, objective='ms', competition_mask = competition_mask_0)
        revenue_cur    = RF.pred_revenue(assortment, objective='rv')
        markets_cur    = RF.pred_revenue(assortment, objective='ms')
        return revenue_cur_ab,markets_cur_ab,revenue_cur_rt,markets_cur_rt,revenue_cur,markets_cur

    
    def revenue_optimization_dual(self, df, schema_str):
        ## from df we extract the different parameters
        wslr = df[wslr_col].iloc[0]
        yrmo = df[yrmo_col].iloc[0]
        results = pd.DataFrame(columns=schema_str.names)
        L_p = df['L_p'].iloc[0]
        vpb = df['vpb'].iloc[0]
        npr = df['npr'].iloc[0]
        market_share_threshold = 1 - df['ms_kept'].iloc[0]
        expand = df['expand'].iloc[0]
        append_results = self.define_append_results(wslr, yrmo, npr = npr)

        print("INDEX: wslr %s, yrmo %s, vpb %s, L_p %s, npr %s"%( wslr, yrmo, vpb, L_p, npr))
        print("PARAMETERS: \t", self.max_iter, self.size_block, self.n_restart_per_bissection, self.n_bissection, self.t)

        
        if True:#TRY EXCEPT

            ## from df we extract the values of the utilities... to torch tensors. Size of each tensor is 1*n_products
            df, util_sample_u, util_sample, penal_sample, price_sample, presence_sample = self.data_to_tensor(df)
            
            ## filter necessary products
            units_sample_allowed, units_map_index_allowed, units_sample_index_allowed, \
            util_sample_allowed, price_sample_allowed, presence_sample_allowed = \
              self.filter_products(df, util_sample_u, util_sample, penal_sample, price_sample, presence_sample)

            ## create cnl_allowed
            cnl_allowed = self.create_cnl_object(units_sample_allowed, units_sample_index_allowed)
                
            #For competition products, set their mask to 0
            competition_mask,competition_mask_0= self.define_competition_masks(
            	price_sample_allowed, units_sample_allowed, units_map_index_allowed,units_ab_from_ab)
            
            ## The REV_CNL_with_business_const class is called with the allowed objects and tensors
            RF = REV_CNL(cnl_allowed, units_map_index_allowed,
                         util_sample_allowed, price_sample_allowed, presence_sample_allowed,
                         price_transformation=self.price_transformation,
                         no_purchase_rate=npr, volume_price_balance=vpb,
                         index_nz = index_nz,
                         competition_mask = competition_mask
                        )

            ## Calculate the revenue and the market share for the current assortment
            revenue_cur_ab,markets_cur_ab,revenue_cur_rt,markets_cur_rt,revenue_cur,markets_cur = \
                self.get_objectives(RF, RF.assortment_cur, competition_mask, competition_mask_0)
            
            
            #Generate constraints based on need AND block of coordinates to sample for FW
            A,b,L = self.generate_matrix_constraints(RF, L_p, competition_mask)
            allowed_block = self.generate_block_elements(RF, units_map_index_allowed, units_ab_from_ab)
            
            ## call the zero function optimizer class
            SF = zero_function_optimizer_LP(RF.revenue_from_presence, RF.units_nz, A, b)  
            

            
            ## For L_p == 0 append the values corresponding to the current assortment
            if True:
                results = append_results(results, 0., 0., 0., 0., 0., 0., markets_cur, revenue_cur,
                                         np.array(RF.assortment_cur),
                                         markets_cur_ab, revenue_cur_ab, markets_cur_rt, revenue_cur_rt
                                         )

            if True:
                # For each bissection step, run multiple restarts, score best objective, and update results/bounds on dual
                vpb_low = 0.
                vpb_high = 1.
                vpb_cur = vpb_low
                for bissection_step in range(self.n_bissection):
                    print('DUAL-PATH: step, vpbs', bissection_step, vpb_cur, vpb_low, vpb_high)
                    RF.set_vpb(vpb_cur)
                    set_seed(bissection_step)

                    xs = []
                    objs = []
                    for restart in range(self.n_restart_per_bissection):  
                        ## set the vpb
                        RF.set_vpb(vpb_cur)

                        ## generate an initial x 
                        x_init = SF.x_init_from_x(rd.rand(len(units_sample_allowed)), A, b)
                        #x_init = SF.x_init_rand_w(A, b, m=.7); x_init = SF.x_init_from_x(x_init, A, b)

                        ## generate the frank wolfe output. This output will not be 0s and 1s but it will be somewhere in between (0,1)
                        fw_output = SF.frank_wolf(
                          x_init, A, b, max_iter=self.max_iter, min_samples=self.min_samples, t=self.t,
                          size_block = np.min([self.size_block, len(allowed_block)]), allowed_block=allowed_block
                        )
                        ## get the 0, 1 output from the frank wolfe output
                        x_final = SF.x_init_from_x(fw_output, A, b)
                        ## get the product ids from x_final
                        s_final = SF.x_to_S(x_final)
                        ## append x_final to xs and pred_revenue to pred_revenue
                        xs.append(x_final)
                        objs.append(RF.pred_revenue(list(s_final)))
                        
                        ## calculate the revenue, ms and append results
                        revenue_fin_ab,markets_fin_ab,revenue_fin_rt,markets_fin_rt,revenue_fin,markets_fin = \
                            self.get_objectives(RF, list(s_final), competition_mask, competition_mask_0)
                        
                        seed = restart
                        results = append_results(
                            results, vpb_cur, L, L_p, market_share_threshold, expand, seed, 
                            markets_fin, revenue_fin, s_final,
                            markets_fin_ab, revenue_fin_ab, markets_fin_rt, revenue_fin_rt
                            )
                        
                    ## choose the run from the restarts which has the best objective 
                    seed = np.argmax(objs)
                    x_final = xs[seed]
                    s_final = SF.x_to_S(x_final)
                    revenue_fin_ab,markets_fin_ab,revenue_fin_rt,markets_fin_rt,revenue_fin,markets_fin = \
                            self.get_objectives(RF, list(s_final), competition_mask, competition_mask_0)
                    
                    ## make an update of the bisection step
                    if markets_fin < markets_cur * (1 - market_share_threshold):
                        vpb_low = vpb_cur
                    else:
                        if vpb_cur == 0.:
                            vpb_high /= 2.
                        else:
                            vpb_high = vpb_cur
                    vpb_cur = (vpb_low + vpb_high) / 2

            return results

        ## default output if try breaks
        else: 
            d = pd.DataFrame({
                wslr_col: [wslr], yrmo_col: [yrmo_col],
                'vpb': vpb, 'npr': [npr], 'L': [0], 'L_p': [0], 'ms_threshold': [0], 'expand': [0], 'seed': [47],
                'pred_revenue': [47], 'pred_ms': [47],
                unit: [np.array([47])]
            })
            return d

    def get_assortments(self, df_spark, save_name):

        ## Define the pandas udf  
        @pandas_udf(self.schema_str, PandasUDFType.GROUPED_MAP)
        def revenue_optimization_udf(df):
            return self.revenue_optimization_dual(df, self.schema_str)

        ## Do a spark groupby and the apply the udf on each group and solve in parallel
        ## Each retailer, L_p, vpb, npr, ms_kept, expand combination is split up and solved in parallel
        a = df_spark.groupby(index + ['L_p', 'vpb', 'npr', 'ms_kept']).apply(revenue_optimization_udf)

        write_spdf(a, save_name)

        print('saved: ', save_name)