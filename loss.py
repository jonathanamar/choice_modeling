import pandas as pd
import numpy as np
import torch
import torch.nn.functional as F
import torch.optim as optim

C = 1e10

#Losses
#-------------------
def mse_loss(model, y, yhat):
	losses = F.mse_loss(y, yhat, reduction = 'none')
	losses.sum(1)
	return losses.mean()

def lognormal_loss(model, y, yhat):
	return mse_loss(model, y.log(), yhat.log(), weight=weight)

def get_logloss_reg(weight = None):
	if weight == None:
		return lognormal_loss
	else:
		def logloss_reg(model, y, yhat):
			return mse_loss(model, y.log(), yhat.log()) + weight * torch.sum(torch.abs(model.weight))
		return logloss_reg


def get_kl_div():
	def kl_div(model, y_hat, y, biais = 1/C):
		#input_tensor, output_tensor = split_input(buoy_tensor)
		reduction = 'batchmean'
		return F.kl_div((y+biais).log(), y_hat   , reduction=reduction)
	return kl_div

def get_log_like(negative = False):
	n = 1 if not negative else -1
	def log_like(model, y_hat, y, biais = 1/C):
		return ((y_hat+biais).log() * y).sum(1).mean() * n
	return log_like


def get_metric(model, y_hat, y, criterion, baseline=None):
	if baseline == None:
			baseline = y.mean(0).unsqueeze(0).repeat(y.shape[0],1)
	return (
		criterion(model, y_hat, y).item(),
		criterion(model, baseline, y).item(),
		)

def get_r2(biais = 1/C, n =1.):
	def r2_assortment(model, y_hat, y):
		in_assortment = (y>biais)*1.
		num_products = in_assortment.sum(1).unsqueeze(1)
		r2_baseline = (1./num_products).repeat(1,y.shape[1]) * in_assortment
		
		SST = ((y - r2_baseline)**2).sum(1)
		SSE = ((y - y_hat)**2).sum(1)
		
		R2 = (1-SSE/SST).clamp(0,1).mean()
		return R2 * n
	return r2_assortment

r2_assortment = get_r2()
